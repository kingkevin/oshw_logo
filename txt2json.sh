#!/usr/bin/env sh
cp cv_json_format.txt cv_json_format.json
# remove comments
sed -i 's://.*$::g' cv_json_format.json
# remove trailing spaces
sed -i 's/\s*$//g' cv_json_format.json
# remove lines with whitespace
sed -i '/^$/d' cv_json_format.json
# verify json
cat cv_json_format.json | json_verify
